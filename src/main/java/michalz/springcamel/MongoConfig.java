package michalz.springcamel;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.UnknownHostException;

/**
 *
 */
@Configuration
public class MongoConfig {


  @Bean
  public Mongo localDb() throws UnknownHostException {
    Mongo mongo = new MongoClient();
    return mongo;
  }
}
