package michalz.springcamel;

import michalz.springcamel.routes.SbcRouteBuilder;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.component.servlet.CamelHttpTransportServlet;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
public class CamelConfig {

  private static final String CAMEL_URL_MAPPING = "/camel/*";
  private static final String CAMEL_SERVLET_NAME = "CamelServlet";

  @Bean
  public ServletRegistrationBean servletRegistrationBean() {
    ServletRegistrationBean registrationBean = new ServletRegistrationBean(new CamelHttpTransportServlet(), CAMEL_URL_MAPPING);
    registrationBean.setName(CAMEL_SERVLET_NAME);
    return registrationBean;
  }

  @Bean
  public SpringCamelContext camelContext(ApplicationContext applicationContext) throws Exception {
    SpringCamelContext camelContext = new SpringCamelContext(applicationContext);
    camelContext.addRoutes(routeBuilder());
    return camelContext;
  }

  @Bean
  public RoutesBuilder routeBuilder() {
    return new SbcRouteBuilder();
  }

}
