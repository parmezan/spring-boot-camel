package michalz.springcamel.web;

import lombok.extern.slf4j.Slf4j;
import michalz.springcamel.domain.PublicAttributes;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequestMapping("/rs/")
@Slf4j
public class RestApiController {


  @RequestMapping("hello")
  public String sayHello() {
    return "HelloWorld!";
  }


  @RequestMapping(value = "publicInfo", consumes = "application/json", produces = "application/json", method = RequestMethod.PUT)
  public String processPublicAttributes(@RequestBody PublicAttributes publicAttributes) {
    log.info("Processing: {}", publicAttributes);

    return "OK!";
  }
}
