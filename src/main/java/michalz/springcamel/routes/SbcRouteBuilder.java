package michalz.springcamel.routes;

import michalz.springcamel.components.PersonIdGenerator;
import michalz.springcamel.components.PersonSplitter;
import michalz.springcamel.domain.Person;
import michalz.springcamel.domain.PersonalInfo;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;

/**
 *
 */
public class SbcRouteBuilder extends RouteBuilder {

  @Override
  public void configure() throws Exception {

    restConfiguration().component("servlet").bindingMode(RestBindingMode.json);

    Endpoint mongoEndpoint = endpoint("mongodb:localDb?database=sbc_test&collection=personalInfo&createCollection=true&operation=save");

    JacksonDataFormat personsListFormat = new JacksonDataFormat(Person.class);
    personsListFormat.setUseList(true);


    rest("/rest/person").put().type(Person.class).route()
        .log("log:restLog?level=INFO")
        .to("direct:personValidation");

    from("direct:personValidation")
        .to("bean-validator://person").to("direct:person");

    from("file:///tmp/persons").unmarshal(personsListFormat)
        .split().simple("${body}")
        .to("log:personLog?level=INFO")
        .to("direct:person");

    from("direct:person").bean(PersonIdGenerator.class)
        .split().method(PersonSplitter.class)
        .choice()
        .when().body(PersonalInfo.class)
          .to(mongoEndpoint)
        .otherwise()
          .marshal().json(JsonLibrary.Jackson)
          .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.PUT))
          .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
          .to("http4://localhost:8080/rs/publicInfo/?bridgeEndpoint=true");
  }
}
