package michalz.springcamel.components;

import lombok.extern.slf4j.Slf4j;
import michalz.springcamel.domain.Person;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.UUID;

/**
 *
 */
@Component
@Slf4j
public class PersonIdGenerator {

  public Person addId(Person person) throws UnsupportedEncodingException {
    UUID generatedId = UUID.nameUUIDFromBytes(person.getEmailAddress().getBytes("UTF-8"));
    person.setGeneratedId(generatedId.toString());
    return person;
  }
}
