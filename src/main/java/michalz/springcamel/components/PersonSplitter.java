package michalz.springcamel.components;

import lombok.extern.slf4j.Slf4j;
import michalz.springcamel.domain.Person;
import michalz.springcamel.domain.PersonalInfo;
import michalz.springcamel.domain.PublicAttributes;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 *
 */
@Component
@Slf4j
public class PersonSplitter {

  public List<Object> splitPerson(Person person) {
    return Arrays.asList(
        PersonalInfo.fromPerson(person), PublicAttributes.fromPerson(person)
    );
  }

}
