package michalz.springcamel.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 *
 */
@Data
public class PublicAttributes {

  private String generatedId;

  //common data
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date registrationDate;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date lastLoginDate;

  public static PublicAttributes fromPerson(Person person) {
    PublicAttributes pa = new PublicAttributes();
    pa.setGeneratedId(person.getGeneratedId());
    pa.setLastLoginDate(person.getLastLoginDate());
    pa.setRegistrationDate(person.getRegistrationDate());
    return pa;
  }
}
