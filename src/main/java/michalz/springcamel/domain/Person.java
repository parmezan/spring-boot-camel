package michalz.springcamel.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 *
 */
@Data
public class Person {

  private String generatedId;

  //personal data
  @NotNull
  @Size(min = 2)
  private String firstName;

  @NotNull
  @Size(min = 2)
  private String lastName;

  @NotNull
  private String emailAddress;

  //common data
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date registrationDate;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date lastLoginDate;
}
