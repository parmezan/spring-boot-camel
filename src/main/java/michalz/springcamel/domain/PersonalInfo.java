package michalz.springcamel.domain;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 */
@Data
public class PersonalInfo {
  private String generatedId;

  //personal data
  @NotNull
  @Size(min = 2)
  private String firstName;

  @NotNull
  @Size(min = 2)
  private String lastName;

  @NotNull
  private String emailAddress;

  public static PersonalInfo fromPerson(Person person) {
    PersonalInfo pi = new PersonalInfo();
    pi.setGeneratedId(person.getGeneratedId());
    pi.setFirstName(person.getFirstName());
    pi.setLastName(person.getLastName());
    pi.setEmailAddress(person.getEmailAddress());
    return pi;
  }
}
