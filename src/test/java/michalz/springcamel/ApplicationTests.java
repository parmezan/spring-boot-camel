package michalz.springcamel;

import michalz.springcamel.domain.Person;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.spring.CamelBeanPostProcessor;
import org.apache.camel.spring.SpringCamelContext;
import org.apache.camel.test.spring.MockEndpoints;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, ApplicationTests.AdditionalConfig.class})
@WebAppConfiguration
@MockEndpoints("mongodb:*")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@IntegrationTest
public class ApplicationTests {

	@Autowired
	private SpringCamelContext springCamelContext;

	@EndpointInject(uri = "direct:personValidation")
	private ProducerTemplate producerTemplate;

	@Test
	public void contextLoads() {
		Assert.assertNotNull(springCamelContext);
	}

	@Test(expected = CamelExecutionException.class)
	public void shouldFailOnEmptyObject() throws Exception {
		Person person = new Person();
		producerTemplate.sendBody(person);
	}

	@Test
	public void testPerson() throws Exception {
		Person person = createRealPerson();
		producerTemplate.sendBody(person);

	}

	private Person createRealPerson() {
		Person person = new Person();
		person.setFirstName("FirstName");
		person.setLastName("LastName");
		person.setEmailAddress("email@address.com");
		person.setRegistrationDate(Date.from(LocalDateTime.of(2014, 1, 1, 12, 10).atZone(ZoneId.systemDefault()).toInstant()));
		person.setLastLoginDate(Date.from(LocalDateTime.of(2014, 8, 12, 12, 10).atZone(ZoneId.systemDefault()).toInstant()));
		return person;
	}

	@Configuration
	static class AdditionalConfig {

		@Bean
		public CamelBeanPostProcessor camelBeanPostProcessor() {
			return new CamelBeanPostProcessor();
		}
	}
}
